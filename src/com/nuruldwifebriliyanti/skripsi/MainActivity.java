package com.nuruldwifebriliyanti.skripsi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.nuruldwifebriliyanti.skripsi.database.DataHelper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;


 
public class MainActivity extends AppCompatActivity implements OnClickListener{
	Button belajar,bermain,keluar;

	private DataHelper datahelper;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
       
        
        belajar = (Button) findViewById(R.id.btn_belajar);
        bermain = (Button) findViewById(R.id.btn_bermain);
        keluar  = (Button) findViewById(R.id.btn_keluar);
        belajar.setOnClickListener(this);
        bermain.setOnClickListener(this);
        keluar.setOnClickListener(this);
        datahelper = new DataHelper(this);
        
       
    }

       

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stu
		int id = arg0.getId();
		if (id == R.id.btn_belajar){
			Intent intent = new Intent(this, MainFragment.class);
			startActivity(intent);
		}else if (id == R.id.btn_bermain){
			Intent intent = new Intent(this, KuisActivity.class);
			startActivity(intent);
		}else if (id == R.id.btn_keluar){
			keluar();
		}
	}
	
	@Override
	public void onBackPressed()
	{
		keluar();
		return;
	}
	private void keluar(){
		new AlertDialog.Builder(this)
		.setTitle("Keluar")
        .setMessage("Are you sure you want to exit?")
        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).setNegativeButton("Tidak", null).show();
	}
}
